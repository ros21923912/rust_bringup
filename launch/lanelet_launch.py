from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='lanelet_rs',
            executable='lanelet_publisher'
        ),
        Node(
            package='lanelet_rs',
            executable='lanelet_path_cross'
        )
    ])
